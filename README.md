# BiometricLibrary

[![CI Status](https://img.shields.io/travis/luciana sorbelli/BiometricLibrary.svg?style=flat)](https://travis-ci.org/luciana sorbelli/BiometricLibrary)
[![Version](https://img.shields.io/cocoapods/v/BiometricLibrary.svg?style=flat)](https://cocoapods.org/pods/BiometricLibrary)
[![License](https://img.shields.io/cocoapods/l/BiometricLibrary.svg?style=flat)](https://cocoapods.org/pods/BiometricLibrary)
[![Platform](https://img.shields.io/cocoapods/p/BiometricLibrary.svg?style=flat)](https://cocoapods.org/pods/BiometricLibrary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BiometricLibrary is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BiometricLibrary'
```

## Author

luciana sorbelli, luciana@cryptouno.app

## License

BiometricLibrary is available under the MIT license. See the LICENSE file for more info.

//
//  ViewController.swift
//  BiometricLibrary
//
//  Created by luciana sorbelli on 03/15/2022.
//  Copyright (c) 2022 luciana sorbelli. All rights reserved.
//

import UIKit
import BiometricLibrary
import LocalAuthentication

class ViewController: UIViewController {

    let params = BiometryParameters.init(context: LAContext(), reason: "Biometria", requestPassMsg: "Some biometry message", fallbackTitle: "Retry biometry")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let auth = BiometricAuthorization.init(parameters: params)
        auth.authenticationWithTouchID() { (result) in
            switch result {
            case .success(let result):
                print(result)
            case .failure(let error):
                print(error)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


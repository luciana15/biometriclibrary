//
//  BiometricLogin.swift
//  BiometricLibrary
//
//  Ask for TouchID/FaceID
//  If Biometrics only then should fallback to custom passcode or username/password.
//  If it not Biometrics then should ask for iPhone Passcode.
//

import Foundation
import LocalAuthentication

public struct BiometryParameters {
    public let context: LAContext
    public let reason: String
    public let requestPassMsg: String
    public let fallbackTitle: String
    
    public init(context: LAContext, reason: String, requestPassMsg: String, fallbackTitle: String ){
        self.context = context
        self.reason = reason
        self.requestPassMsg = requestPassMsg
        self.fallbackTitle = fallbackTitle
      }
}

public enum BiometryError: Error {
    case authenticationFailed
    case appCancel
    case invalidContext
    case notInteractive
    case passcodeNotSet
    case systemCancel
    case userCancel
    case userFallback
    case biometryNotAvailable
    case biometryLockout
    case biometryNotEnrolled
    case touchIDLockout
    case touchIDNotAvailable
    case touchIDNotEnrolled
}

open class BiometricAuthorization: NSObject {
    
    private let parameters: BiometryParameters
    private var error: NSError?
    
    public init(parameters: BiometryParameters) {
        self.parameters = parameters
    }

    public func authenticationWithTouchID(_ completion: @escaping (Result< Bool?, BiometryError>) -> Void) {
        self.parameters.context.localizedFallbackTitle = parameters.fallbackTitle
        if self.parameters.context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            self.parameters.context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: self.parameters.reason) {  [weak self] success, evaluateError in
                guard let self = self else { return }
                if success {
                        completion(.success(success))
                        return
                    } else {
                        guard let error = evaluateError else {
                            return
                        }
                        completion(.failure(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code)))
                    }
                }
        } else {
            guard let error = error else {
                return
            }
            completion(.failure(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code)))
        }
    }
        
    private func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> BiometryError {
        switch errorCode {
            case LAError.authenticationFailed.rawValue: return .authenticationFailed
            case LAError.appCancel.rawValue: return .appCancel
            case LAError.invalidContext.rawValue: return .invalidContext
            case LAError.notInteractive.rawValue: return .notInteractive
            case LAError.passcodeNotSet.rawValue: return .passcodeNotSet
            case LAError.systemCancel.rawValue: return .systemCancel
            case LAError.userCancel.rawValue: return .userCancel
            case LAError.userFallback.rawValue: return .userFallback
        default: return evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
        }
    }
    
    private func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> BiometryError {
        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
                case LAError.biometryNotAvailable.rawValue: return .biometryNotAvailable
                case LAError.biometryLockout.rawValue: return .biometryLockout
                case LAError.biometryNotEnrolled.rawValue: return .biometryNotEnrolled
                default: return .biometryNotAvailable
                }
        } else {
            switch errorCode {
                case LAError.touchIDLockout.rawValue: return .touchIDLockout
                case LAError.touchIDNotAvailable.rawValue: return .touchIDNotAvailable
                case LAError.touchIDNotEnrolled.rawValue: return .touchIDNotEnrolled
                default: return .touchIDNotAvailable
            }
        }
    }
}
